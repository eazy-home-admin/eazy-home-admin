FROM ubuntu:22.04

# Update and install all required packages in a single RUN command
RUN apt-get update && apt-get install -y \
    ffmpeg \
    libsm6 \
    libxext6 \
    openssl \
    neofetch \
    python3 \
    autoconf \
    pkgconf \
    gcc \
    flex \
    patch \
    make \
    m4 \
    cmake \
    python3-pip \
    automake \
    libatlas-base-dev \
    libgtk-3-dev \
    libboost-all-dev \
    && rm -rf /var/lib/apt/lists/*

# Set the working directory
WORKDIR /usr/src/app

# Copy the current directory contents into the container at /usr/src/app
COPY . .

# List the contents of the working directory (for debugging purposes)
RUN ls

# Install Python dependencies
RUN pip3 install --no-cache-dir -r requirements.txt

# Expose the desired port
EXPOSE 5000

# Run the application
CMD ["python3", "./init.py"]
