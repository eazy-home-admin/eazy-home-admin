"""
Design Patterns required for the framework.

"""



class Singleton(type):
    """
    MetaClass for Singleton Design pattern
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class FinalClass(type):
    """
    MetaClass for classes that do not allow subclasses (Final class)
    """
    def __new__(cls, name, bases, class_dict):
        for base in bases:
            if isinstance(base, FinalClass):
                raise TypeError(f"Cannot subclass {base.__name__}")
        return super().__new__(cls, name, bases, class_dict)


class SingletonNoSubClass(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonNoSubClass, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

    def __new__(mcs, name, bases, attrs):
        for base in bases:
            if isinstance(base, Singleton):
                raise TypeError(f"Inheritance of singleton class '{base.__name__}' is not allowed.")
        return super(SingletonNoSubClass, mcs).__new__(mcs, name, bases, attrs)
