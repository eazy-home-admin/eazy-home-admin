import os
import unittest
from Environment.APIEnvironment import UnTrustedExtensionExecutor


class TestUnTrustedExtensionExecutor(unittest.TestCase):
    def setUp(self):
        self.obj = None

    def test_basic(self):
        with open('/home/sivarajan/PycharmProjects/eazy-home-server/Environment/tests/codes/basic.py', 'r') as f:
            code = f.read()

        env = UnTrustedExtensionExecutor()
        f = env.get_class(code,'x')


        self.obj = f()
        self.assertTrue(hasattr(self.obj, 'foo'), "Class x should have a method foo.")

        res = self.obj.foo()
        self.assertEqual(res, 'foo')
        # Assert if file is created
        self.assertTrue(os.path.exists('/home/sivarajan/Desktop/res.txt'))




if __name__ == "__main__":
    unittest.main()
