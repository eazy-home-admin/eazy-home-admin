import socket

def start_server():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost', 12345))
    s.listen(1)
    print("Server is listening on port 12345...")

    conn, addr = s.accept()
    print(f"Connection established with {addr}")

    try:
        while True:
            data = conn.recv(1024)
            if data == 'exit':
               break
            print(f"Received: {data.decode('ascii')}")
            conn.send("pong".encode("ascii"))
            print("Sent: pong")

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        conn.close()
        s.close()
        print("Server has been shut down.")

if __name__ == "__main__":
    start_server()
