import fcntl
import os
import shutil
import tempfile
import cv2
import joblib

import yaml


class Configuration:
    """
    This class manages the configuration for the application.

    It loads the configuration from a YAML file, and provides methods to save and load the
    configuration.

    Attributes:
        CONFIG_FILE (str): The path to the YAML configuration file.
        USER_DATA_DIR (str): The path to the user's data directory.
        LOCK_FILE (str): The path to the lock file.
    """
    USER_DATA_DIR = os.path.join(os.path.expanduser('~'), '.config', 'eazy-home-server')
    CONFIG_FILE = os.path.join(USER_DATA_DIR, 'config.yaml')
    LOCK_FILE = os.path.join(USER_DATA_DIR, 'config.lck')

    def __init__(self,read_only=False):
        self.read_only = read_only
        self.no_of_workers = None
        self.use_telegram = None
        self.debug = None
        self.port = None
        self.set_https = None
        self.load_configuration()


    def load_configuration(self):
        """
        Load configuration from yaml file
        """
        config = {}
        if os.path.exists(Configuration.CONFIG_FILE):
            with open(Configuration.CONFIG_FILE, 'r') as file:
                config = yaml.safe_load(file)
                self.use_telegram = config.get('use-telegram-api', False)
                self.no_of_workers = config.get('no_of_workers', 1)
                self.debug = config.get('debug', False)
                self.port = config.get('port', 5000)
                self.set_https = config.get('use-https', False)
        else:
            # Handle missing config file by setting default values or creating a new config
            print(f"Configuration file not found at {Configuration.CONFIG_FILE}. Using default values.")
            # check if path exist
            if not os.path.exists(Configuration.USER_DATA_DIR):
                os.makedirs(Configuration.USER_DATA_DIR)
            self.use_telegram = False
            self.no_of_workers = 1
            self.debug = False
            self.port = 5000
            self.set_https = False
            self.save_configuration({
                'use-telegram-api': self.use_telegram,
                'no_of_workers': self.no_of_workers,
                'debug': self.debug,
                'port': self.port,
                'use-https': self.set_https
            })

    def save_configuration(self, data: dict):
        """
        Save configuration to yaml file
        Parameters: 
            data (dict): data to be saved
        """
        if self.read_only:
            return
        if not os.path.exists(Configuration.USER_DATA_DIR):
            os.makedirs(Configuration.USER_DATA_DIR)

        lock_file = open(Configuration.LOCK_FILE, 'w')
        try:
            fcntl.flock(lock_file, fcntl.LOCK_EX)
            # Load existing config   if exists
            config = {}
            if os.path.exists(Configuration.CONFIG_FILE):
                with open(Configuration.CONFIG_FILE, 'r') as file:
                    config = yaml.safe_load(file)

            # Update config with new data
            config['no_of_workers'] = data.get('no_of_workers', self.no_of_workers)
            config['use-telegram-api'] = data.get('use-telegram-api', self.use_telegram)
            config['debug'] = data.get('debug', self.debug)
            config['port'] = data.get('port', self.port)
            config['use-https'] = data.get('use-https', self.set_https)

            # Write to a temporary file first
            with tempfile.NamedTemporaryFile('w', delete=False, dir=Configuration.USER_DATA_DIR) as temp_file:
                yaml.dump(config, temp_file)
                temp_file_path = temp_file.name

            # Atomically move the temp file to the config file location
            shutil.move(temp_file_path, Configuration.CONFIG_FILE)
        finally:
            fcntl.flock(lock_file, fcntl.LOCK_UN)
            lock_file.close()



