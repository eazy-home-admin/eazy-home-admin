import fcntl

import yaml
import os

from design_patterns.patterns import FinalClass



class ACLManager(metaclass=FinalClass):
    """
    This class manages the access control list (ACL) for the module in a restricted environment.

    Attributes:
        _acl_config (dict): A dictionary containing the ACL configuration.
        _config_path (str): The path to the ACL configuration file.
        _read_only (bool): A flag indicating whether the ACLManager instance is read-only.
    """
    def __init__(self, config_path=None,
                 read_only=True, content_if_not_exists=None):
        if config_path is None:
            if os.path.exists(os.path.join(os.path.expanduser('~'), '.config', 'eazy-home-server')) is False:
                os.makedirs(os.path.join(os.path.expanduser('~'), '.config', 'eazy-home-server'))

            config_path = os.path.join(os.path.expanduser('~'), '.config', 'eazy-home-server', 'acl.yaml')
        if os.path.exists(os.path.abspath(config_path)) is False and content_if_not_exists is not None:
            with open(config_path, 'w') as file:
                yaml.dump(content_if_not_exists, file)
        self._config_path = config_path
        self._read_only = read_only
        self._acl_config = self.load_acl_config()

    def load_acl_config(self):
        with open(self._config_path, 'r') as file:
            return yaml.safe_load(file)

    def save_acl_config(self):
        if not self._read_only:
            with open(self._config_path, 'w') as file:
                # Acquire an exclusive lock on the file
                fcntl.flock(file, fcntl.LOCK_EX)
                yaml.safe_dump(self._acl_config, file)
                # Release the lock
                fcntl.flock(file, fcntl.LOCK_UN)
        else:
            raise PermissionError('ACL Manager is read-only')

    def add_allowed_directory(self, directory):
        directory = os.path.abspath(directory)
        if directory not in self._acl_config.get('Allowed Directories', []):
            self._acl_config.setdefault('Allowed Directories', []).append(directory)
            self.save_acl_config()

    def remove_allowed_directory(self, directory):
        directory = os.path.abspath(directory)
        if directory in self._acl_config.get('Allowed Directories', []):
            self._acl_config['Allowed Directories'].remove(directory)
            self.save_acl_config()

    def is_allowed_directory(self, directory):
        directory = os.path.abspath(directory)
        return directory in self._acl_config.get('Allowed Directories', [])

    def add_allowed_library(self, library):
        if library not in self._acl_config.get('Allowed Libraries', []):
            self._acl_config.setdefault('Allowed Libraries', []).append(library)
            self.save_acl_config()

    def remove_allowed_library(self, library):
        if library in self._acl_config.get('Allowed Libraries', []):
            self._acl_config['Allowed Libraries'].remove(library)
            self.save_acl_config()

    def is_allowed_library(self, library):
        return library in self._acl_config.get('Allowed Libraries', [])

    def add_trusted_extension(self, extension):
        if extension not in self._acl_config.get('Trusted Extensions', []):
            self._acl_config.setdefault('Trusted Extensions', []).append(extension)
            self.save_acl_config()

    def remove_trusted_extension(self, extension):
        if extension in self._acl_config.get('Trusted Extensions', []):
            self._acl_config['Trusted Extensions'].remove(extension)
            self.save_acl_config()

    def is_trusted_extension(self, extension):
        return extension in self._acl_config.get('Trusted Extensions', [])

    def add_trusted_url(self, url):
        """
        Add a trusted URL to the ACL config.
        Parameters:
            url (str): The URL to add to the ACL config.
        """
        if url not in self._acl_config.get('Trusted URLs', []):
            self._acl_config.setdefault('Trusted URLs', []).append(url)
            self.save_acl_config()

    def remove_trusted_url(self, url):
        if url in self._acl_config.get('Trusted URLs', []):
            self._acl_config['Trusted URLs'].remove(url)
            self.save_acl_config()

    def is_trusted_url(self, url):
        return url in self._acl_config.get('Trusted URLs', [])

    def get_trusted_urls(self):
        return self._acl_config.get('Trusted URLs', [])

    def get_allowed_directories(self):
        return self._acl_config.get('Allowed Directories', [])

    def get_allowed_libraries(self):
        return self._acl_config.get('Allowed Libraries', [])

    def get_trusted_extensions(self):
        return self._acl_config.get('Trusted Extensions', [])

    def read_only_clone(self):
        return ACLManager(config_path=self._config_path, read_only=True)

    def rw_clone(self):
        return ACLManager(config_path=self._config_path, read_only=False)