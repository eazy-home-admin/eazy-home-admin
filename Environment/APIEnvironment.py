"""This Module defines the Environment that is required for the device definition and other extensions in the future.
It also contains safer implementations of libraries like OS, Socket for the UnTrusted Extensions to execute within
the environment.  This module also takes care of security in regards external libraries.
"""

import ast
import RestrictedPython
import yaml
from RestrictedPython.transformer import INSPECT_ATTRIBUTES, astStr, copy_locations
from RestrictedPython import safe_builtins, compile_restricted, PrintCollector
from RestrictedPython.Eval import default_guarded_getiter
from RestrictedPython.Guards import guarded_iter_unpack_sequence
import importlib

import os

from Device.core.device import Device
from Environment.SafeLibs import SafeOS, SafePandasDataFrame, SafeCV2, SafeJoblib, SafeFile
from Environment.acl_manager import ACLManager


SAFE_MODULES = []
# Custom AST node transformer for RestrictedPython, enhancing security by
# limiting the operations and attributes that user code can access.
class ModifiedNTransformer(RestrictedPython.RestrictingNodeTransformer):
    """
        A custom AST node transformer for RestrictedPython that enhances security by
        limiting the operations and attributes accessible to user-defined code.

        Attributes:
            errors (list): A list to collect syntax errors found during the compilation.
            warnings (list): A list to collect warnings during the compilation.
            used_names (dict): A dictionary tracking names used in the user code to prevent conflicts.
        """

    def __init__(self, errors=None, warnings=None, used_names=None):
        super().__init__(errors, warnings, used_names)

    # Restricts attribute access to prevent unsafe operations.
    def visit_Attribute(self, node):

        """
           Restricts attribute access within user-defined code to prevent access to
           unsafe or protected attributes.

           Args:
               node (ast.Attribute): The attribute node to be inspected and potentially transformed.

           Returns:
               ast.Call: A transformed node that safely accesses the attribute,
                   or the original node if no transformation is needed.
        """
        # Disallow access to attributes starting with '_' except for '__init__'
        if node.attr.startswith('_') and node.attr not in ['_', '__init__']:
            self.error(node, f'"{node.attr}" is an invalid attribute name because it starts with "_".')

        # Disallow attributes that end with '__roles__'
        if node.attr.endswith('__roles__'):
            self.error(node, f'"{node.attr}" is an invalid attribute name because it ends with "__roles__".')

        # Enforce restrictions on accessing certain Python attributes.
        if node.attr in INSPECT_ATTRIBUTES:
            self.error(node, f'"{node.attr}" is a restricted name, forbidden in RestrictedPython.')

        # Custom processing for attribute access (load/store/delete)
        if isinstance(node.ctx, ast.Load):
            node = self.node_contents_visit(node)
            new_node = ast.Call(func=ast.Name('_getattr_', ast.Load()), args=[node.value, astStr(node.attr)],
                                keywords=[])
            copy_locations(new_node, node)
            return new_node
        elif isinstance(node.ctx, (ast.Store, ast.Del)):
            node = self.node_contents_visit(node)
            new_value = ast.Call(func=ast.Name('_write_', ast.Load()), args=[node.value], keywords=[])
            copy_locations(new_value, node.value)
            node.value = new_value
            return node
        else:
            raise NotImplementedError(f"Unknown ctx type: {type(node.ctx)}")


# Defines the modules that are considered safe to import in the restricted environment.

# Custom function to control module imports, allowing only safe modules.


# Mimics the write function in a controlled manner.
def _write_(x):
    return x


# Executes user code within a restricted and controlled environment.
class UnTrustedExtensionExecutor():
    """
       Executes user code within a restricted and controlled environment, allowing for
    the safe execution of dynamically loaded extensions.

    Attributes:
        my_globals (dict): A dictionary defining the global environment in which the user code is executed.
        SAFE_MODULES (set): A set of modules that are considered safe to import in the restricted environment.
    """

    SAFE_MODULES = None

    def __init__(self):

        self._acl_manager = ACLManager(read_only=False, content_if_not_exists={
            'Allowed Directories': ['/tmp/'],
            'Allowed Libraries': ['scipy', 'matplotlib', 'extension', 'math', 'random', 'sklearn', 'code_p.py', 'numpy',
                                  'seaborn', 'keras', 'tensorflow'],
            'Trusted Extensions': ['Arduino'],
            'Trusted URLs': ['127.0.0.1']
        })  # To Verify Permissions.
        # Convert all the acl list attributes to


        # safe modules is frozen set of trusted libraries from acl.
        self.SAFE_MODULES = frozenset(self._acl_manager.get_allowed_libraries())

        self.my_globals = {
            "__builtins__": {
                **safe_builtins,
                "__import__": self._safe_import,
                "_print_": PrintCollector,
                '_getattr_': getattr,
                '_setattr_': setattr,
                '_write_': _write_,
                'dict': dict,
                'list': list,
                'set': set,
            },
            '__metaclass__': type,
            '__name__': f'extensions',
            '_iter_unpack_sequence_': guarded_iter_unpack_sequence,
            '_getiter_': default_guarded_getiter,
            'object': object,
            'open': self.safe_open,
            'os': SafeOS,
            # 'socket': SafeSocket,
            'yaml':yaml,
            'print': PrintCollector,
            'super': super,
            'Device': Device,
            'acl_manager': self._acl_manager.read_only_clone(),
            'File': SafeFile
        }


        print(SAFE_MODULES)

    @staticmethod
    def safe_open(x,y):
        raise PermissionError("File access disallowed in this method. use File class instead.")
    def _safe_import(self, module_name, *args, **kwargs):
        """
            A custom import function that restricts the modules that can be imported in the restricted environment.

            Args:
                module_name (str): The name of the module to import.
                *args: Variable length argument list.
                **kwargs: Arbitrary keyword arguments.

            Returns:
                module: The imported module if it's deemed safe.

            Raises:
                Exception: If the module is not in the list of safe modules.
            """
        if not isinstance(module_name, str):
            raise TypeError(f"Module name must be a string, not {type(module_name)}")

        if module_name not in self.SAFE_MODULES:
            raise Exception(f"Import of {module_name!r} is not allowed as extension is untrusted")
        print("import success")
        return __import__(module_name, *args, **kwargs)

    # Global variables for the execution environment, including safe built-ins and custom functions.

    # Compiles and executes user code, returning the defined class by name.
    def get_class(self, user_code, class_name: str):
        """
        Compiles and executes the provided user code, returning the class defined within that code.

        Args:
            user_code (str): The user-provided code to be executed in the restricted environment.
            class_name (str): The name of the class to retrieve from the executed code.

        Returns:
            class: The class object defined in the user code, if found.

        Raises:
            SyntaxError: If there's a syntax error in the user-provided code.
        """
        try:
            self.my_globals['__name__'] = class_name
            # Set the module name dynamically based on class name.
            byte_code = compile_restricted(user_code, filename="<user_code>", mode="exec", policy=ModifiedNTransformer)
            exec(byte_code, self.my_globals)
            return self.my_globals.get(class_name, None)
        except SyntaxError as e:
            raise e


class TrustedExtensionExecutor:
    """
    Loads and returns classes from trusted extensions using the importlib module.

    This executor is intended for use with extensions that have been vetted and are
    considered safe to execute without the restrictions applied by RestrictedPython.
    """

    def __init__(self, extensions_dir='extensions'):
        """
        Initializes the TrustedExtensionExecutor with the directory containing the extensions.

        Args:
            extensions_dir (str): The directory where extension modules are stored. Defaults to 'extensions'.
        """
        self.extensions_dir = extensions_dir

    def load_class(self, module_name, class_name):
        """
        Loads the specified class from the given module within the extensions directory.

        Args:
            module_name (str): The name of the module file (without the .py extension).
            class_name (str): The name of the class to load from the module.

        Returns:
            class: The class object specified by class_name from the imported module.

        Raises:
            ImportError: If the module or class cannot be found.
            AttributeError: If the class cannot be found in the imported module.
        """
        try:
            # Construct the module import path
            module_path = f"{self.extensions_dir}.{module_name}"
            # Import the module
            module = importlib.import_module(module_path)
            # Return the class
            return getattr(module, class_name)
        except ImportError as e:
            print(f"Error importing module: {e}")
            raise
        except AttributeError as e:
            print(f"Error finding class {class_name} in module: {e}")
            raise

    def get_class(self, module_name, class_name):
        """
        Imports the specified class from the given module within the extensions' directory.

        Parameters:
            module_name (str): The name of the module file (without the .py extension).
            class_name (str): The name of the class to import from the module.

        Returns:
            class: The class object specified by class_name from the imported module.

        Raises:
            ImportError: If the module or class cannot be found.
            AttributeError: If the class cannot be found in the imported module.
        """
        try:
            # Construct the module import path
            module_path = f"{self.extensions_dir}.{module_name}"
            # Import the module
            module = importlib.import_module(module_path)
            # Return the class
            return getattr(module, class_name)
        except ImportError as e:
            print(f"Error importing module: {e}")
            raise
        except AttributeError as e:
            print(f"Error finding class {class_name} in module: {e}")
            raise

# This method is work in progress
