import RPi.GPIO as GPIO

from Device.core.device import Device


@Register.action
class RaspberryPiDevice(Device):
    """
    Represents a Raspberry Pi device, providing methods to control its GPIO pins.

    This class extends the Device class, adding specific properties and actions
    to interact with the Raspberry Pi's hardware interfaces.

    Attributes inherited from Device:
        name (str): The name of the device. Default is None.
        id (str): A unique identifier for the device. Default is None.
        actions (list of str): A list of action names that the device can perform.
        category (str): The category of device. Set to "Active" for Raspberry Pi.
    """

    def __init__(self, *args, **kwargs):
        """
        Initializes a new instance of the Raspberry Pi device class, setting up
        the GPIO interface.
        """
        super().__init__(*args, **kwargs)
        self.type = "Active"  # Raspberry Pi is an active device
        GPIO.setmode(GPIO.BCM)  # Set the GPIO pins to BCM numbering
        GPIO.setwarnings(False)  # Disable GPIO warnings
        self.pin_states = {}  # Initialize pin states

    def setup_pin(self, pin, direction):
        """
        Sets up a GPIO pin as INPUT or OUTPUT.

        :param pin: Pin number based on BCM numbering.
        :param direction: GPIO.IN for input, GPIO.OUT for output.
        """
        GPIO.setup(pin, direction)
        self.pin_states[pin] = {'mode': direction, 'state': GPIO.LOW}

    def write_pin(self, pin, state):
        """
        Writes a HIGH or LOW value to a GPIO pin.

        :param pin: Pin number based on BCM numbering.
        :param state: GPIO.HIGH to set the pin "on", GPIO.LOW to set "off".
        """
        if pin not in self.pin_states:
            raise ValueError("Pin not configured. Please set it up first.")
        GPIO.output(pin, state)
        self.pin_states[pin]['state'] = state
        return f"Pin {pin} set to {'HIGH' if state else 'LOW'}."

    def read_pin(self, pin):
        """
        Reads the value of a GPIO pin.

        :param pin: Pin number based on BCM numbering.
        :return: GPIO.HIGH if the pin is "on", GPIO.LOW if "off".
        """
        if pin not in self.pin_states or self.pin_states[pin]['mode'] != GPIO.IN:
            raise ValueError("Pin not configured for input.")
        return GPIO.input(pin)

    def turn_on(self, pin):
        """Turns on a device connected to the given pin."""
        return self.write_pin(pin, GPIO.HIGH)

    def turn_off(self, pin):
        """Turns off a device connected to the given pin."""
        return self.write_pin(pin, GPIO.LOW)

    def cleanup(self):
        """
        Cleans up by resetting all the GPIO pins that have been used by this program.
        """
        GPIO.cleanup()


# Example usage:
# Assuming there is an LED on pin 18
raspberry_pi = RaspberryPiDevice(name="RaspberryPi1", id="RPi01")
raspberry_pi.setup_pin(18, GPIO.OUT)
raspberry_pi.turn_on(18)
# Do something with the LED
raspberry_pi.turn_off(18)
raspberry_pi.cleanup()
