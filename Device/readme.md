

# Device Package

## Overview

The Device package is an integral component of Eazy Home Admin, facilitating communication between the server and various Internet of Things (IoT) devices. Currently, this package predominantly supports Arduino via Serial Port Communication, with plans to extend support to other IoT devices.

### Note
This project is in active development and has not been fully integrated with the main application yet.

## Device Class

The `Device` class serves as a foundational template for creating subclasses tailored to specific devices, establishing essential attributes and methods for seamless interaction with our server.

### Key Attributes:

- `actions` (Static Member) : Lists the actions that a device can perform, expressed as strings.
- `name`: The device's name.
- `id`: A unique identifier for the device.
- `_access_urls` (Static Member): Specifies URLs the device may access. Not mentioned URLs will be inaccessible, enhancing security.
- `_access_directories` (Static Member): Outlines directories the device may access. Ensuring sensitive directories are not exposed or accessed without explicit whitelisting.

### Creating Custom Device Classes

To integrate a new device, developers should subclass the `Device` class, overriding methods and attributes to encapsulate the device's specific functionalities.

Note: all the custom functions must receive arguments as dict.  This is being done to pass database values easier.
#### Example:

```python
from Device.core.device import Device, Register

@Register.action  # This decorator helps in automatically registering device actions.
class LightPassiveDevice(Device):
    """A subclass of Device demonstrating basic actions for a light device."""
    
    # Specify URLs that the device is allowed to access.
    _access_urls = ['http://example.com/data', 'https://api.example.org/status']
    
    # Define directories that the device can access. 
    _access_directories = ['/var/lib/devices/light', '/tmp/device_cache']

    def turn_on(self,args:dict):
        """Turns the light on."""
        return "Light turned on."

    def turn_off(self, args:dict):
        """Turns the light off."""
        return "Light turned off."

    def change_brightness(self, args:dict):
        """Adjusts the light's brightness.
        
        Args:
            brightness (int): Desired brightness level.
        """
        return f"Light brightness set to {args.get('Brightness',None)}."

    @Register.ignore
    # Methods marked with @Register.ignore won't be recognized as device actions.
    def dance(self):
        """A method not intended as a device action."""
        return "Dance functionality not available."
```

### Whitelisting URLs and Directories

The static members `_access_urls` and `_access_directories` play crucial roles in security, allowing developers to explicitly specify which resources are accessible to a device. Any attempt to access URLs or directories not listed in these members will be blocked, ensuring devices cannot interact with untrusted or potentially harmful resources.

This whitelist approach ensures that devices operate within a controlled environment, reducing the risk of security vulnerabilities that may arise from unrestricted access to network resources or the filesystem.

### Implementing Your Device Classes

When defining your device classes, it's important to utilize the `@Register.action` and `@Register.ignore` decorators to manage which methods are exposed as actions. This structured approach helps maintain a clear separation between actionable device functionalities and internal methods, further enhancing the security and reliability of the IoT server framework.

