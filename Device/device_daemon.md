

# Device Daemon Documentation
**Note:** This is a plan. The work towards the daemon goes on when we have finalized the stuffs.
## Overview
The Device Daemon is a standalone background service responsible for direct communication with various hardware devices. It listens for command requests, validates them, and executes the corresponding actions on the devices.
This is core module required for normal functioning of the Eazy-Home-Admin
## Architecture

- **Command Listener**: The daemon has a listener component that listens on a designated IPC (Inter-Process Communication) channel or Unix Socket for incoming command requests.

- **Command Processor**: Upon receiving a command, the processor parses the command, validates the digital signature, and translates it into actions for specific devices.

- **Device Registry**: A central repository that keeps track of all connected devices, their IDs, types, and statuses.

- **Device Drivers**: Individual modules within the daemon that know how to communicate with specific types of hardware (e.g., Arduino, Raspberry Pi).

- **Security Module**: A component responsible for the verification of digital signatures to ensure commands are from authorized sources.

- **Response Handler**: Once a command is executed, the response handler formats the outcome and sends it back to the requester.

## Command Request Structure
- **DeviceID**: A unique identifier for each device.
- **Command**: The action to be performed, such as `turn_on`, `turn_off`, etc.
- **Arguments**: Additional parameters required to perform the action.
- **Signature**: A digital signature for validating the authenticity of the command.

## Command Processing Flow
1. **Receive Request**: The daemon listens for incoming requests on a Unix Socket or other IPC mechanism.
2. **Parse Request**: Break down the request into `DeviceID`, `Command`, `Arguments`, and `Signature`.
3. **Validate Signature**: The security module checks the digital signature against the known public key of the client.
4. **Look Up Device**: The device registry is queried with `DeviceID` to retrieve the device instance.
5. **Execute Command**: The device driver associated with the device type executes the command.
6. **Send Response**: The outcome is returned to the requester.

## Digital Signature Implementation
- **Key Generation**: The daemon and its clients will have a pair of public and private keys.
- **Signing Commands**: Clients sign their commands with their private key before sending.
- **Verification**: The daemon uses the client's public key to verify the signature upon receipt.

## Command format

```
	DeviceID;command ( turn_on,turn_off,or others);Args
	
```

Where:
- DeviceID: Unique Device/controller Identifier.
- Command: command to be executed on device.
- Args: Additional supplementary Arguments

Design Choice: A Command Interpreter is required so that each of the arguments can be split and parsed. 
The functions that are designed to work must be registered as actions in the class as well as the argument count must be valid.
And As design choice it should be thread-safe. This will be a great choice to make the job good.

### Device Organization and Lookup Mechanism

The device ecosystem within our project is structured to cater to a wide range of IoT devices, including sensors, actuators, and controllers. To maintain a clear and efficient system for managing these devices, we've established a robust organization and lookup mechanism detailed below.

#### Device ID Organization:

Devices are uniquely identified through a systematic ID prefix that indicates their category:

- **Controllers**: Devices that manage one or more other devices are prefixed with 'C'. Example: `C100`.
- **Standalone Devices/Sensors**: Devices capable of operating independently, such as sensors, are prefixed with 'S'. Example: `S101`.
- **Non-Standalone Devices**: Devices that require a controller to operate are prefixed with 'N'. Example: `N102`.

This ID system facilitates quick identification of the device type, streamlining the lookup and interaction processes.

### Device Lookup Mechanism:

The device lookup mechanism is designed to streamline interactions with IoT devices by leveraging metadata stored in the EazyDB. This structured approach enables the system to efficiently identify and interact with devices, whether they are standalone, controlled by a separate controller, or part of a broader device ecosystem.

#### Detailed Proposal for Device Lookup:

1. **Verification of Device Existence**:

   - Upon receiving a request to interact with a specific device, the system first verifies whether the device is listed in the EazyDB. If the device is not found, the system raises an exception, indicating the absence of the device in the database.

2. **Device Type Determination**:
   - The system examines the device's unique identifier or prefix to categorize it as a standalone device, a controller, or a non-standalone device. This categorization is pivotal in deciding the subsequent steps for action execution.

3. **Execution of Device Action**:
   - **For Standalone Devices**: If identified as standalone, the system directly loads the corresponding class responsible for the device and executes the requested action, utilizing the arguments provided in the device's dictionary entry from the DBMS.
   - **For Non-Standalone Devices**: If the device operates under a controller, the system retrieves the controller's details, loads the controller class, and executes the requested action by providing the necessary parameters from the device's dictionary entry in the DBMS.
   - In both scenarios, the classes access their required parameters through the dictionary passed to them, which contains the device's entry from the DBMS. This ensures that only the Device Daemon has direct access to the DBMS, maintaining a secure environment for device interaction.
   - Upon successful execution of device action, The device daemon expects dictionary, which will be used to appropriate database key value.
4. **Validation of Metadata and Schema**:
   - During device registration, the system consults the Device class to determine the required fields and data needed for storage. This ensures that all device interactions adhere to a predefined schema, enhancing the integrity and reliability of device operations.


#### Example:
1. **Command Reception**: The Device Daemon receives a command structured as `N1;set_brightness;255`.
2. **Command Parsing**:
   - The Command Interpreter splits the command into components:
     - Device ID: `N1`
     - Command: `set_brightness`
     - Arguments: `[255]`
3. **Device Existence Verification**: The Device Daemon queries the database to verify the existence of the device with ID `N1`.
4. **Controller Identification for Non-Standalone Devices**:
   - If the device is identified as non-standalone, the Daemon retrieves the controller's ID associated with this device.
5. **Device Details Retrieval**: The Daemon fetches device details from the database, assembling them into a dictionary for easy access. For example:
   ```yaml
   name: lamp1
   added_on: 2023-08-10 14:39:42.507175
   id: N3
   pin: 13
   status: 'off'
   controller: C1
   type: actuator
   updated_on: 2023-12-11 22:00:57.491205
   usage: 2957.433333333334
   ```
6. **Appending Arguments**:
   - The command's arguments are appended to the details dictionary under the key `args`:
     ```yaml
     args:
       - 255
     ```
7. **Class Instantiation**:
   - The appropriate class for the device's controller is instantiated based on the `controller` field in the details dictionary:
     ```python
     controller_instance = devices_list['C1']
     ```
8. **Action Execution**:
   - The device action is executed by passing the details dictionary to the controller instance's method:
     ```python
     response = controller_instance.execute_action("set_brightness", args)
     ```
     - Inside the `execute_action` method, the device-specific arguments are extracted and used to perform the action.

9. **Handling Execution Response**:
   - The response from the execution (success, failure, or status update) can then be processed accordingly, potentially involving updating the device's status in the database or relaying the outcome back to the requester.


## Security Considerations
- **Key Storage**: Keys should be stored securely using a key management system.
- **Access Control**: Only allow connections from known clients with registered public keys.
- **Audit Trail**: Keep logs of all commands received and actions taken for traceability.


## Additional Considerations for Device Daemon

### Parallel Processing with Task Queues
To enhance performance and manage asynchronous tasks efficiently, it's essential to consider integrating with task queue systems. Systems like **Celery** and **Redis Queue (RQ)** are robust options for handling concurrent operations and can distribute the load across multiple worker nodes. This approach ensures that tasks such as device state updates or long-running device control commands do not block the main execution flow of the device daemon.

### Dynamic Class Loading for Device Control
Considering the ever-growing variety of IoT devices, our architecture must support the seamless integration of new devices. By storing external device control classes in a designated `Device/extensions` directory, we facilitate an extensible plug-and-play approach. The device daemon can dynamically load these classes at runtime, thus enabling the support of new devices without the need for core application updates. This modular architecture allows for greater flexibility and scalability as the ecosystem of supported devices expands.

## Future Enhancements

### NLP-Based Command Interpreter
The future roadmap includes developing an NLP-based command interpreter that allows users to interact with their devices using natural language. This system will interpret the user's spoken or written instructions, accurately discerning the intent and translating them into specific device actions. Leveraging advanced NLP techniques will provide a more intuitive and accessible way for users to manage their home automation systems.

### Custom Command-Action Mapping
To cater to the unique needs of different users, there is a plan to design a customizable command-action mapping system. This system would enable users to define their own command phrases and link them to specific actions on their devices. A user-friendly interface could be provided for setting up these mappings, backed by a robust service for storing and processing the user-defined configurations.

### Optimized Device Search Mechanism
As the number of connected devices in a smart home grows, so does the need for an efficient search mechanism. The device daemon will incorporate optimized search algorithms, indexing strategies, and caching mechanisms to facilitate the rapid discovery of devices. This optimization will be crucial for maintaining quick response times and ensuring a seamless user experience, even in environments with a large array of IoT devices.
### Device Organization and Lookup Mechanism

The device ecosystem within our project is structured to cater to a wide range of IoT devices, including sensors, actuators, and controllers. To maintain a clear and efficient system for managing these devices, we've established a robust organization and lookup mechanism detailed below.

#### Device ID Organization:

Devices are uniquely identified through a systematic ID prefix that indicates their category:

- **Controllers**: Devices that manage one or more other devices are prefixed with 'C'. Example: `C100`.
- **Standalone Devices/Sensors**: Devices capable of operating independently, such as sensors, are prefixed with 'S'. Example: `S101`.
- **Non-Standalone Devices**: Devices that require a controller to operate are prefixed with 'N'. Example: `N102`.

This ID system facilitates quick identification of the device type, streamlining the lookup and interaction processes.


## TODO List

- [ ] Determine further challenges.
- [ ] Initial Device Daemon Implementation.
- [ ] Simple Daemon and API Broker Communication.
- [ ] Implement Digital Signature and Encryption Channel.
- [ ] Develop a Sensor Monitoring System with Threading.
