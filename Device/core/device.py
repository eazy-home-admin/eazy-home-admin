"""
Class defintions for device API. All the device definitions must adhere to this class.
Copyright (c) 2024 Sivarajan R.
This software is distributed under AGPL License. See the LICENSE file in the root of this distribution.
"""

import inspect


class RequiredArgs:
    """
       Encapsulates the properties of a required argument for a device.

       Attributes:
           name (str): The name of the argument.
           description (str): A brief description of what the argument represents.
           default_value: The default value of the argument.
           required (bool): Indicates whether the argument is mandatory.
       """

    def __init__(self, name, description, default_value, required):
        self.name = name
        self.description = description
        self.default_value = default_value
        self.required = required

    def __str__(self):
        return f"{self.name},{self.description},{self.default_value},{self.required}"

    def __getitem__(self, item):
        if item == 0:
            return self.name
        elif item == 1:
            return self.description
        elif item == 2:
            return self.default_value
        elif item == 3:
            return self.required
        else:
            raise KeyError('Invalid Key')


class Register:
    """
    A static Class to contain all the decorators to register the device.
    """

    @staticmethod
    def ignore(func):
        """
        Decorator to mark a method to be ignored; it should not be treated as an action.
        """
        func._ignore = True
        return func

    @classmethod
    def action(cls, target_cls):
        """
        Class decorator that registers callable attributes of the class as actions,
        except those marked with @ignore.
        """
        target_cls.actions = []  # Initialize or reset the 'actions' list

        for attr_name, attr_value in vars(target_cls).items():
            # Skip methods marked with @ignore
            if getattr(attr_value, '_ignore', False):
                continue

            # Check if the attribute is a callable (ignoring dunder methods)
            if callable(attr_value) and not attr_name.startswith('__'):
                target_cls.actions.append(attr_name)

        return target_cls


class Device:
    """
    Represents a generic device, providing a framework for executing device-specific actions.
    
    This class serves as a base for devices, defining common properties and methods that can be
    overridden by subclasses to implement device-specific behavior.
    
    Attributes:
        name (str): The name of the device. Default is None.
        id (str): A unique identifier for the device. Default is None.
        actions (list of str): A list of action names that the device can perform. Default actions
                               include 'turn_on', 'turn_off', and 'change_brightness'.
        category (str): The category of device. Default is None.
        room_id (str): The room number of the  device. Defaults to 0
        controller_id(int): ID of the controller
    """

    actions = []  # action lookup Register
    _access_urls = []  # Advertise the URLS that can be accessed beforehand.
    _access_directories = []  # Directories that can be accessed by URLS

    def __init__(self, **kwargs):
        """
           Initializes a new instance of the Device class.

           Parameters:
               - name (str): The name of the device.
               - id (int): A unique identifier for the device.
               - category (str): The category of device.
               **kwargs: Arbitrary keyword arguments.
                Supported keywords:
                   - room_id (str): The room number of the  device. Defaults to 0
                   - controller_id(int): ID of the controller
           """

        self.name = kwargs.get('name', "UnknownDevice")
        # Align action names with method names
        self.id = kwargs.get('id', 'x')

        self.category = kwargs.get('category', 'Actuator')

        self.is_controller = kwargs.get('is_controller', False)

        self.controller_id = kwargs.get('controller_id', None)

        self.room_id = kwargs.get('room_id', 0)

    @Register.ignore
    def execute_action(self, action, args:dict):
        """
          Executes a specified action on the device.

          If the action is not recognized or not implemented, an appropriate message is returned.

          Parameters:
              action (str): The name of the action to be executed.
              args (dict): The arguments to be sent as dictionary.

          Returns:
              str: The result of the action execution or an error message.
          """
        if action in self.actions:  # Use self.actions to refer to the subclass's actions list
            method = getattr(self, action, None)
            if method and not getattr(method, '_ignore', False):  # Check if method is not marked to ignore
                return method(args)
            else:
                return f"Action {action} not implemented."
        else:
            return "Invalid action."

    def __str__(self):

        attr_strings = [f"{name}:{value}" for name, value in inspect.getmembers(self) if
                        not callable(value) and not name.startswith('_')]

        return "\n".join(attr_strings)

    @Register.ignore
    def to_dict(self):
        """Converts the class attribute to dictionary.
        :returns: dict: Dictionary of the class attributes.
        """
        return {name: value for name, value in inspect.getmembers(self)
                if not callable(value) and not name.startswith('_')}

    @staticmethod
    def required_arguments() -> list[RequiredArgs]:
        """
        Constructs and returns a list of RequiredArgs instances, each representing a required argument
        for device registration and configuration within the system. This method defines a foundational
        set of arguments applicable to all device classes. These arguments are essential not only for
        device registration but also for updating device configurations in the database.

        Subclasses should extend this list with their specific required arguments to ensure comprehensive
        attribute availability in their respective dictionaries. It is recommended to combine the output
        of this function with that of the overridden method in subclasses. This practice ensures that the
        registration and configuration dictionary includes both general and device-specific attributes
        necessary for accurate and effective device management.

        Returns:
            list[RequiredArgs]: A list of RequiredArgs instances, with each instance detailing
                                the name, description, default value, and requirement status of
                                a registration and configuration attribute.
        """
        return [
            RequiredArgs('name', 'Name of the device, used for identification in the database and during registration.',
                         None, True),
            RequiredArgs('id', 'Unique ID of the device, critical for database operations and device management.',
                         None, True),
        ]
