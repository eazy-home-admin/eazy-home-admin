import os, pty
from serial import Serial
import threading


class ArduinoSimulator:
    # Define device names, pins, and states as per the Arduino sketch
    """
        A simulator for Arduino devices that supports basic digital and analog pin operations.
        This class allows for simulating the behavior of Arduino boards (Uno, Mega) by handling
        digital and analog read/write operations. It uses pseudo-terminals to simulate serial communication,
        making it a useful tool for development and testing without requiring physical hardware.

        Attributes:
            boards (dict): Supported Arduino boards with their digital and analog pin limits.
            board (str): The category of Arduino board being simulated ('Uno' or 'Mega').
            pin_states (dict): Current state of all digital and analog pins.
            _master_id (int): File descriptor for the master end of the pseudo-terminal.
            _slave_id (int): File descriptor for the slave end of the pseudo-terminal.
        """
    boards = {'Uno': {'digital_limit': 14, 'analog_limit': 6},
              'Mega': {'digital_limit': 54, 'analog_limit': 16}}

    def __init__(self, board='Uno'):
        """
        Initializes the ArduinoSimulator with the specified board category.

        :param:  board The category of Arduino board to simulate. Defaults to 'Uno'.

        :raises ValueError: If an unsupported board is provided.
        """
        if board not in self.boards:
            print(f"Unsupported board category: {board}")

        self.board = board
        self._dig_limit = self.boards[board]['digital_limit']
        self._ana_limit = self.boards[board]['analog_limit']
        self.pin_states = {"digital": {}, "analog": {}}
        self._master_id, self._slave_id = pty.openpty()  # Opening pseudo terminals
        self._terminate = False

    def get_ids(self):
        return self._master_id, self._slave_id

    def get_tty_name(self):
        return os.ttyname(self._master_id), os.ttyname(self._slave_id)

    def is_valid_pin(self, pin, mode='digital'):
        """Check if the pin is valid for the selected board."""
        if mode == 'digital':
            return 0 <= pin < self._dig_limit
        elif mode == 'analog':
            return 0 <= pin < self._ana_limit
        else:
            print("Invalid mode: mode should be 'digital' or 'analog'")

    def convert_analog_pin(self, pin_str):
        """Convert an analog pin string (e.g., 'A0') to its pin."""
        if not pin_str.upper().startswith('A') or not pin_str[1:].isdigit():
            return None
        pin_num = int(pin_str[1:])
        return pin_num if self.is_valid_pin(pin_num, mode='analog') else None

    def digital_write(self, pin, state):
        if not self.is_valid_pin(pin):
            print("Invalid digital pin")
        self.pin_states["digital"][pin] = state
        print(f"Digital pin {pin} set to {state}")

    def digital_read(self, pin):
        if not self.is_valid_pin(pin):
            print("Invalid digital pin")
        return self.pin_states["digital"].get(pin, 0)

    def analog_write(self, pin, value):
        if not self.is_valid_pin(pin, mode='analog'):
            print("Invalid analog pin")
        self.pin_states["analog"][pin] = value
        print(f"Analog pin {pin} set to {value}")

    def analog_read(self, pin):
        if not self.is_valid_pin(pin, mode='analog'):
            print("Invalid analog pin")
        return self.pin_states["analog"].get(pin, 0)

    def process_command(self, command):
        """Process a command from the serial interface."""
        split_command = command.split(':')
        if len(split_command) != 2:
            return "Invalid Argument supplied."

        action, value = split_command

        if action.startswith("A"):  # Handling analog pin references
            pin = self.convert_analog_pin(action)
            if pin is None:
                return "Invalid Analog Pin"
            action = 'analog'
        else:
            try:
                pin = int(action)
                if not self.is_valid_pin(pin):
                    return "Invalid Digital Pin"
                action = 'digital'
            except ValueError:
                return "Invalid Pin Format"

        if action == 'analog':
            if value == "read":
                return str(self.analog_read(pin))
            else:
                try:
                    value = int(value)
                    self.analog_write(pin, value)
                    return f"Analog pin {pin} set to {value}"
                except ValueError:
                    return "Invalid value for analog write"
        else:  # Digital action
            if value == "on":
                self.digital_write(pin, 1)
                return "The device has been turned on"
            elif value == "off":
                self.digital_write(pin, 0)
                return "The device has been turned off"
            else:
                return "Invalid command for digital write"

    def __listener(self, port):
        """Listen for commands on the given port."""
        try:
            while not self._terminate:
                res = b""
                while not res.endswith(b"\r\n"):
                    res += os.read(port, 1)
                print("Command received: %s" % res)
                response = self.process_command(res.decode().strip())
                os.write(port, response.encode())
                print(f"{response} has been sent")
            os.close(self._master_id)
            os.close(self._slave_id)
        except Exception as e:
            print(f"Error in listener: {e}")

    def run_serial_threaded(self):
        """Run the simulator in a separate thread."""
        print(f"Slave device name: {os.ttyname(self._slave_id)}")
        print(f"Master device ID: {self._master_id}")
        thread = threading.Thread(target=self.__listener, args=[self._master_id])
        thread.start()

    def terminate(self):
        self._terminate = True



class ArduinoSimulatorLegacy:
    """
    A simple simulator for an Arduino device that manages and controls
    a set of simulated devices (e.g., lamps, fans, door) through serial communication.

    This class listens for commands to read the state of a device or to turn a device on or off.
    The simulator uses pseudo-terminals to simulate the serial communication typically found
    in Arduino projects.

    Attributes:
        device_list (dict): A dictionary mapping device names to their pin numbers and states.
    """

    def __init__(self, **kwargs):

        self.device_list = kwargs.get('device_list', {
            'lamp1': {'pin': 9, 'state': 0},
            'lamp2': {'pin': 10, 'state': 0},
            'fan1': {'pin': 11, 'state': 0},
            'fan2': {'pin': 12, 'state': 0},
            'door': {'pin': 3, 'state': 0}  # Example device
        })
        self._master_id, self._slave_id = os.openpty()

    def get_id(self):
        return self._master_id, self._slave_id

    def get_pty_names(self):
        return os.nam

    def listener(self, port):
        """
        Listens for commands on the given port and processes them accordingly.

        Args:
            port (int): The file descriptor of the master end of the pseudo-terminal.
        """
        try:
            while True:
                res = b""
                while not res.endswith(b"\r\n"):
                    res += os.read(port, 1)
                print(f"Command received: {res}")
                self.__process_command(res.decode().strip(), port)
        except Exception as e:
            print(f"Listener encountered an error: {e}")

    def __process_command(self, command, port):
        """
        Processes a command received from the serial interface and performs the requested action.

        Args:
            command (str): The command to process.
            port (int): The file descriptor where the response will be written.
        """
        loc = command.find(':')
        if loc <= 0:
            response = "Invalid Argument supplied.\r\n"
        else:
            device_name, action = command[:loc], command[loc + 1:]
            device = self.device_list.get(device_name)
            if device:
                if action == "read":
                    state = device['state']
                    response = f"State: {state}\r\n"
                elif action in ["on", "off"]:
                    device['state'] = 1 if action == "on" else 0
                    response = f"Info: {device_name} turned {action}\r\n"
                else:
                    response = "Error: Invalid Action;\r\n"
            else:
                response = "Error: Invalid Device\r\n"

        os.write(port, response.encode())

    def run_serial_threaded(self):
        """
        Sets up a pseudo-terminal for serial communication and starts the listener
        in a separate thread. This method prints the slave device name for connection.
        """
        thread = threading.Thread(target=self.listener, args=[self._master_id], daemon=True)
        thread.start()
    def get_tty_name(self):
        return os.ttyname(self._master_id), os.ttyname(self._slave_id)
