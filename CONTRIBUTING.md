### CONTRIBUTING for Eazy Home Server

Welcome to the Eazy-Home-Server contribution guide! Thank you for your interest in contributing to our project. 

#### 🚀 How to Contribute

- **Fork the repository:** Start by forking the this repository to your Gitlab account. This provides you a personal copy where you can make changes before submitting them.

- **Clone your forked repository:** Clone the repository to your local machine to start working on the changes.

- **Create a new branch:** Always create a new branch for your changes. This helps in managing different features or fixes independently.

- **Make your changes:** Implement your features or fixes.

- **Write meaningful commit messages:** Your commit messages should clearly describe what has been changed or fixed.


- **Push changes to your fork:** After making your changes and committing them, push them to your fork.

- **Open a pull request:** From your fork, open a pull request to the main  Eazy home repository. Provide a concise and informative title and description for your pull request.

- **Review process:** Your pull request will be reviewed by maintainers and possibly other contributors. Be open to feedback and make the necessary revisions as requested.

- **Usage of Generative AI**: You may utilize Generative AI to assist in development, provided that all AI-generated code is carefully reviewed to ensure it is secure and does not disrupt the software.

#### 📝 Technical Contributions

- **Contact:** Before making substantial changes, please get in touch with Sivarajan R (Owner) or designated Eazy-Home-Server maintainers.

- **Documentation:** Clearly document all changes made. Use Python docstrings for code documentation, adhering to Python standards.

- **Testing:** Add tests for new features and ensure that all tests pass for fixes.

#### 🐞 Bug Reports and Security Vulnerabilities

- **Reporting bugs:** If you find a bug, please open an issue detailing the bug, steps to reproduce it, and, if applicable, include error logs and screenshots.

- **Security vulnerabilities:** For reporting security issues, contact Sivarajan R directly and discreetly to prevent potential exploits before they are patched.

- **Suggestions for fixes:** If you know how to fix an issue, include that in your bug report.

#### 🖋 Non-Technical Contributions

- **Documentation:** Help improve or translate the documentation. Your contributions make it easier for new users and contributors to get started.

- **Community engagement:** Help new users understand how to use Eazy Home Server, answer questions, and participate in discussions.

#### 🌐 Community and Support

- **Join the community:** Participate in Discord Group.

- **Feedback:** Your feedback is valuable to us. Share your experiences with Eazy Home Server,  and suggestions for improvement.

#### 📚 Continuous Documentation

- **Documentation updates:** Keep the documentation up-to-date by revising it alongside development changes.

