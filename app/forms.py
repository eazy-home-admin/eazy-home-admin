# forms.py
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.fields.simple import BooleanField
from wtforms.validators import DataRequired, EqualTo

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')

class RegisterForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')


class SettingsForm(FlaskForm):
    arduinoport = StringField('Arduino Port', validators=[DataRequired()])
    use_telegram = BooleanField('Use Telegram Bot')  # Assuming this is a boolean field, you can change it accordingly
    debug = BooleanField('DEBUG mode')  # Assuming this is a boolean field
    port = StringField('Server Port Number', validators=[DataRequired()])
    use_automatic_door = BooleanField('Use Automatic Door')  # Assuming this is a boolean field
    use_https = BooleanField('Use HTTPS')  # Assuming this is a boolean field
    submit = SubmitField('Submit')

class SettingsForm(FlaskForm):
    arduinoport = StringField('Arduino Port', validators=[DataRequired()])
    use_telegram = BooleanField('Use Telegram Bot')  # Assuming this is a boolean field, you can change it accordingly
    debug = BooleanField('DEBUG mode')  # Assuming this is a boolean field
    port = StringField('Server Port Number', validators=[DataRequired()])
    use_automatic_door = BooleanField('Use Automatic Door')  # Assuming this is a boolean field
    use_https = BooleanField('Use HTTPS')  # Assuming this is a boolean field
    submit = SubmitField('Submit')
