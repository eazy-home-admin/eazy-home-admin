[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/eazy-home-admin/eazy-home-admin)

# Eazy Home Admin
Eazy home admin is meant to be a simple server-side applications for automating your home inside Local Area network It is meant to be a replacement for other Home automation applications.
This project is at its early stage, so it is incomplete. It focuses on simplification of server side setup for the person who needs a simple home automation system. 
We as a community also focus on keeping the code base as simple as possible so that people who are new to code can participate or modify the program as they wish.


This project is done by Sivarajan R under AGPL License.
All kinds of contributions are welcome.

**NOTE:** CURRENT VERSION IN MASTER IS NOT RUNNABLE SINCE A COMPLETE REWRITE IS GOING ON.

# Credits
  - Airsthib - Desgin of admin panel and website of this project.
  - Nirmal Kumar - Desgin of web-client interface.
  - sprtdpd - Unified Device language.
## Description

### Features:
- Simple to use.
- Light weight.
- Encrypted request(SSL).
- Ease control of your home devices using android .
- Face recognition implementation for intrusion detection system.
- All data are stored locally.  
- User Passwords stored encrypted.
- Auto data summary of your power consumption of devices. 
- Expandability: you can expand the list of devices that are supported if you know python.
- This can send a message to telegram when someone used the face-recognition system for door lock.

## Client Apps:
Currently, there is no official client app available. Soon we will have a client app for Android devices that will work with this specific app.

## Help
### System Requirements:
#### Hardware:
- PC with 2 GB RAM (Or Raspberry Pi).
- A LAN Network.
- Arduino UNO / MEGA.

#### Software and Dependencies:
- Linux (Debian and Arch based Distros)


##### Note: 
- This is tested well on Ubuntu LTS and on Raspbian OS (Debian Based distro for Raspberry Pi).It is recommended to use Linux for using the server side script.
- It can run on other linux platforms as well. Check out your Linux distribution repository for dependencies.  
- This may or may not work on Windows. If you want to help the community with making it work well on Windows, Feel free to contact us.

Needed Dependencies:



- Manjaro/Arch Linux:    
  ``` sudo pacman -S openssl python autoconf pkgconf gcc flex patch make guile m4 cmake python-pip automake```
- Debian/UbuntuTo support your income, you can work
  ```sudo apt install python openssl build-essentials```


Python Dependencies (Common for Windows/Linux):

It is recommended to create a virtual environment dedicated to this app. To create a virtual environment type:
```python -m venv venv```

Then install the python dependencies with the following command: 
```./venv/bin/python -m pip install -r requirments.txt```


Since the code is under rewrite, The Programs do not work at all. Take a look at to TO-DO to see a current task and help the community.
