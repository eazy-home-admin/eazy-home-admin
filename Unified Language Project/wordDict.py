#Test bloat words
bloatWords = ["the", "turn", "please"]

#Test unifying dictionary
uniDict ={

    "on" : "enable",
    "enable" : "enable",
    "activate" : "enable",

    "off" : "disable",
    "disable" : "disable",
    "deactivate" : "disable",

    "set" : "set",
    "change" : "set",
    "to" : "set",

    "raise" : "increment",
    "increase" : "increment",
    "add" : "increment",

    "lower" : "decrement",
    "decrease" : "decrement",
    "subtract" : "decrement",
    }
