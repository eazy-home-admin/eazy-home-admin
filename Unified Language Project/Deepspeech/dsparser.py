import text2int
import unifiedCommand


class Break(Exception): pass


class Parser:

    def __init__(self, bloatWords, subjectList, uniDict):
        self.bloatWords = bloatWords
        self.subjectList = subjectList
        self.uniDict = uniDict

    # Cleans up raw text: removes bloat words, formats and removes excess whitespace and converts word numbers to digits
    def prototypeCleanOutput(self, inputText):

        # Fix numbers
        inputText = text2int.text2int(inputText)

        # Remove bloat words
        for i in self.bloatWords:
            inputText = inputText.replace(i, "")

        # Pad both sides of the string with whitespace to allow unifrorm isolation of any given word
        inputText = " " + inputText + " "

        # Remove excess whitespace
        while inputText.find("  ") != -1:
            inputText = inputText.replace("  ", " ")

        return inputText

    def prototypeUnifier(self, inputText):

        # Four part unified command
        command = unifiedCommand.unifiedCommand(None, None, None, None)

        try:

            # Locate a subject
            for i in self.subjectList:

                if inputText.find(i.name) != -1:
                    command.subject = i
                    break

            # Except if no subject is found
            if command.subject == None:
                raise Exception("No subject found!")

            # Locate an action
            for i in self.uniDict.keys():

                if inputText.find((" " + i + " ")) != -1:
                    command.action = self.uniDict[i]
                    break

            # Except if no action is found
            if command.action == None:
                raise Exception("No action found!")

            # Locate a value
            for i in inputText.split():

                if i.isdigit():
                    command.value = int(i)
                    break

            # Locate a label
            for i in command.subject.labels:

                if inputText.find(i) != -1:
                    command.label = i

            return command

        except Exception as error:
            print('Caught self error: ' + repr(error))
            exit
